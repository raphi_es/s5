from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from .models import Matkul
from .forms import Input_Form

def home(request):
    response = {'input_form':Input_Form}
    return render(request, 'main/home.html', response)

def readmatkul(request):
    response = {'input_form':Input_Form}
    matkuls = Matkul.objects.all()
    response['matkuls'] = matkuls
    return render(request, 'main/readmatkul.html', response)

def savename(request):
    form = Input_Form(request.GET or None)
    if (form.is_valid and request.method == 'GET'):
        form.save()
        return HttpResponseRedirect('/')
        
"""
def delete_matkul(request, part_id=None):
    object = Matkul.objects.get(id=part_id)
    object.delete()
    return render(request, 'main/home.html')
"""

def matkul_delete(request, pk):
    matkul = get_object_or_404(Matkul, pk=pk)  # Get your current matkul
    response = {'input_form':Input_Form}
    matkuls = Matkul.objects.all()
    response['matkuls'] = matkuls
    if request.method == 'POST':         # If method is POST,
        matkul.delete()                     # delete the matkul.
                                       # Finally, redirect to the homepage.

    return render(request, 'main/readmatkul.html', response)

def detail_matkul(request):
    response = {'nama', 'sks'}
    return render(request, 'main/detail_matkul.html', response)

def detailmatkul(request,pk):
    matkul = get_object_or_404(Matkul, pk=pk)
    response = {'nama':matkul.display_name, 'sks':matkul.sks}
    return render(request, 'main/detail_matkul.html', response)
