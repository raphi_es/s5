from django import forms
from .models import Matkul

class Input_Form(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ['display_name','sks']
        
    error_messages = {
        'required' : 'Please enter value'
    }
    input_attrs = {
        'type' : 'text',
        'placeholder' : 'Nama Matkul'
    }
    input_attrs1 = {
        'placeholder' : 'Jumlah SKS'
    }

    display_name = forms.CharField(label='', required=True, 
    max_length=27, widget=forms.TextInput(attrs=input_attrs))
    
    
    sks = forms.CharField(label='', required=True, 
    max_length=27, widget=forms.TextInput(attrs=input_attrs1))

